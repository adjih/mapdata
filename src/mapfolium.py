#---------------------------------------------------------------------------
# Managing a map with folium
#---------------------------------------------------------------------------

import numpy as np
import json
import numbers
import random

import folium
import branca
import branca.colormap as cm
import folium.plugins
import vincent
import geopy.distance

import datamgr

#---------------------------------------------------------------------------

GPS_PONT_NEUF = (48.85868, 2.34149) # arbitrary pos next to Paris center

#---------------------------------------------------------------------------

def rec_swap(l):
    if len(l) == 0:
        return l
    if isinstance(l[0], numbers.Number):
        assert len(l) == 2
        return l[1],l[0]
    else:
        return [rec_swap(e) for e in l]

#---------------------------------------------------------------------------

def get_distances(df_part, reference):
    def compute_distance(geo_point_2d):
        lat,lon = [float(x) for x in geo_point_2d.split(",")]
        return geopy.distance.distance((lat,lon), reference).meters
    return df_part["geo_point_2d"].apply(compute_distance)
    
#---------------------------------------------------------------------------
    
OSMFR_TILES_STYLE = "https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
OSMFR_TILES_ATTR = (
    '&copy; Openstreetmap France | &copy; '
    +'<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    +' contributors')
#tiles_style =  "OpenStreetMap" ; attr = None #"Mapbox Control Room" #"Stamen Terrain", "Stamen Toner", "Mapbox Bright", "Mapbox Control Room"
tiles_style = "cartodbpositron" ; attr = None
#tiles_style = "stamentoner"
#tiles_style = "https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png" ; attr='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
#tiles_style = tiles; attr = "no-tile"

class MapManager:
    def __init__(self, external_dir=None, iris=None):
        self.current_map = None
        self.colorscale = None
        self.tiles_style = "cartodbpositron"
        self.tiles_attr = None
        self.external_dir = external_dir
        self._load_iris(iris)

    def _load_iris(self, other_iris):
        if other_iris is None:
            store = datamgr.DataStore(self.external_dir)
            self.iris = datamgr.IrisDataManager(store)
            self.iris._ensure_data()
        else:
            self.external_dir = other_iris.get_store_dir()
            self.iris = other_iris

    def _get_random_value(self,i):
        #random.seed(i)
        return random.random()
        
    def make_map(self, location=GPS_PONT_NEUF, prefer_canvas=False):
        self.colorscale = cm.linear.YlOrRd_09.scale(0, 1)
        self.current_map = folium.Map(
            location=location, 
            tiles=self.tiles_style, attr=self.tiles_attr, zoom_start=15,
            control_scale=True,
            prefer_canvas=prefer_canvas
        )
        return self.current_map

    def add_full_screen_button(self, position="bottomright"):
        folium.plugins.Fullscreen(
            position=position,
            title='Fullscreen',
            title_cancel='Exit',
            force_separate_button=True
        ).add_to(self.current_map)

    def add_colorscale_display(self):
        #folium.LayerControl().add_to(the_map)
        #colormap = cm.linear.Set1_09.scale(0, 1).to_step(10)
        self.colorscale.caption = 'Random values color code'
        self.current_map.add_child(self.colorscale)

    def add_data_many_geojson(self, df_part, group=None):
        # Version with one GeoJson per Area
        # (does not work, with Chrome for more than ~700-800 ?)
        random.seed(1)
        if group is None:
            group = self.current_map
        
        for i,area in df_part.iterrows():    
            folium.GeoJson(
                area["GEO_SHAPE"],
                style_function=lambda feature,i=i: {
                    'fillColor': self.colorscale(self._get_random_value(i)),
                    'color' : 'black',
                    'weight' : 2,
                    'dashArray' : '5, 5'
                    }
            ).add_to(group)

    def add_data_geojson(self, df_part, group=None):
        """ group is a folium feature_group or a map """
        random.seed(1)
        if group is None:
            group = self.current_map
        # Version with one GeoJSON with all the IRIS areas
        geo_json = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "geometry": json.loads(area["GEO_SHAPE"])
                }
                for i,area  in df_part.iterrows()
            ]
        }
        folium.GeoJson(geo_json,
            style_function=lambda feature: {
                'fillColor': self.colorscale(self._get_random_value(feature)),
                'color' : 'black',
                'weight' : 2,
                'dashArray' : '5, 5'
                }
        ).add_to(group)
        return geo_json
   
    def add_data_polygons(self, df_part, group=None):
        """`df_part` is a panda frame"""
        random.seed(1)
        if group is None:
            group = self.current_map
        
        # Version with one Polygon per area
        for i,area in df_part.iterrows():
            lat,lon = [float(x) for x in area["geo_point_2d"].split(",")]
            geo_shape = json.loads(area["GEO_SHAPE"])
            if geo_shape["type"] not in ["Polygon", "MultiPolygon"]:
                print(geo_shape["type"])
                continue
            assert geo_shape["type"] in ["Polygon", "MultiPolygon"]

            area_value = self._get_random_value(i)
            color = self.colorscale(area_value)

            # XXX do it directly (as MultiPolygon)
            for poly_coord_list in geo_shape["coordinates"]: 
                coord_list = rec_swap(poly_coord_list) 
                polygon = folium.Polygon(
                    coord_list,
                    tooltip = (area["NOM_IRIS"]+"<br>"+str(area["DCOMIRIS"])),
                    color = "gray",
                    dash_array = '3,10',
                    weight = 2,
                    fill_color = color,
                    fill_opacity = 0.2
                )
                polygon.add_to(group)

    def _make_random_graph(self):
        N = 100
        multi_iter2 = {
            'x': np.random.uniform(size=(N,)),
            'y': np.random.uniform(size=(N,)),
        }
        scatter = vincent.Scatter(
            multi_iter2, iter_idx='x', height=100, width=200)
        data = json.loads(scatter.to_json())
        v = folium.features.Vega(data, width='100%', height='100%')
        return v

    def add_data_markers(self, df_part, with_circle=False, group=None):
        """`df_part` is a panda frame"""
        random.seed(1)
        if group is None:
            group = self.current_map
        

        for i,area in df_part.iterrows():
            v = self._make_random_graph()
            
            lat,lon = [float(x) for x in area["geo_point_2d"].split(",")]
            geo_shape = json.loads(area["GEO_SHAPE"])
            if geo_shape["type"] not in ["Polygon", "MultiPolygon"]:
                print(geo_shape["type"])
                continue
            assert geo_shape["type"] in ["Polygon", "MultiPolygon"]

            area_value = self._get_random_value(i)
            #color = self.colorscale(area_value)
            if area_value > 0.5:
                icon_color = "red"
            elif area_value > 0.3:
                icon_color = "lightred"
            elif area_value > 0.1:
                icon_color = "orange"
            else:
                icon_color = "beige"
            
            # Possible colors: {'beige', 'lightgreen', 'cadetblue', 'darkblue', 'purple', 'darkpurple', 'lightblue', 'gray', 'black', 'darkred', 'orange', 'blue', 'red', 'white', 'darkgreen', 'lightgray', 'green', 'lightred', 'pink'}
            
            # XXX do it directly (as MultiPolygon)
            for poly_coord_list in geo_shape["coordinates"]: 
                coord_list = rec_swap(poly_coord_list) 
                
                if not with_circle:
                    mk = folium.features.Marker(
                        [lat, lon],
                        icon=folium.Icon(color=icon_color)
                    )
                    p = folium.Popup(area["NOM_IRIS"])
                    group.add_child(mk)
                    mk.add_child(p)
                    p.add_child(v)
                    
                else:
                    p = folium.Popup(area["NOM_IRIS"])                
                    mk = folium.Circle(
                        location=[lat, lon], radius=30, popup=p,
                        color=icon_color,
                        fill_color=icon_color, fill=True)

                    group.add_child(mk)
                    #mk.add_child(p)
                    p.add_child(v)
        
#---------------------------------------------------------------------------
