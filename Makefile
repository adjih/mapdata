#---------------------------------------------------------------------------

PYTHON=python3.7
MYENV=mapenv

-include Makefile.local

#---------------------------------------------------------------------------
# Virtual Python Environment created with `venv`
#---------------------------------------------------------------------------

ACTIVATE=. ${MYENV}/bin/activate

nothing:
	@cat doc/MAKE-MESSAGE.md | sed s+MYENV+${MYENV}+g

${MYENV}: ; ${PYTHON} -m venv ${MYENV}

ensure-env: ; @test -e ${MYENV} || make ${MYENV}

#--------------------------------------------------

# For Ubuntu 18.04, need prior: sudo apt install python3.7 python3.7-venv

venv-install: ensure-env
	@printf "Press <Return> to install packages in a Python virtual env (Ctrl-C to abort):" && read unused
	${ACTIVATE} && ( \
		pip install wheel pipenv jupyter \
		&& pip install pandas geopandas geopy \
		&& pip install bokeh \
		&& pip install folium voila vincent \
		&& pip install ipywidgets ipyleaflet \
                && pip install matplotlib tabulate \
	)
# TODO: others 'pip install' candidates:
# altair vega  pdvega scipy rtree gpxpy mplleaflet geographiclib
# pykdtree
# xlrd descartes

ensure-voila: ; ${ACTIVATE} && { which voila || make venv-install ; }

#--------------------------------------------------

run-notebook:
	${ACTIVATE} && jupyter notebook

run-voila: ensure-voila external/contours-iris-2014.csv
	${ACTIVATE} && cd src && voila MapSimpleFolium.ipynb

#---------------------------------------------------------------------------

iris-csv: ; make external/contours-iris-2014.csv

external/contours-iris-2014.csv:
	@printf "Press <Return> to download IRIS csv (Ctrl-C to abort):" && read unused
	test -e external || mkdir external
	wget https://www.data.gouv.fr/fr/datasets/r/5e2dbbb6-2a2d-42e8-9b42-210669045869 -O external/contours-iris-2014.csv

#---------------------------------------------------------------------------

# "Découpage infracommunal - Table d'appartenance géographique des IRIS"
# This is found on this page: https://www.insee.fr/fr/information/2017499

INSEE_BASE_URL=https://www.insee.fr/fr/statistiques/fichier/2017499/

IRIS_DEF_ZIP=reference_IRIS_geo2019.zip

external/${IRIS_DEF_ZIP}:
	test -e external || mkdir external
	cd external && wget ${INSEE_BASE_URL}/${IRIS_DEF_ZIP}

IRIS_DEF_XLS=reference_IRIS_geo2019.xls

external/${IRIS_DEF_XLS}:
	make external/${IRIS_DEF_ZIP}
	cd external && unzip ${IRIS_DEF_ZIP}

#--------------------------------------------------

IGN_IRIS_PKG=CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z.001

IGN_IRIS_DIR=CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01

IGN_IRIS_AREA_URL=ftp://Contours_IRIS_ext:ao6Phu5ohJ4jaeji@ftp3.ign.fr/CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z.001

external/${IGN_IRIS_PKG}:
	test -e external || mkdir external
	cd external && wget ${IGN_IRIS_AREA_URL}

external/${IGN_IRIS_DIR}:
	make external/${IGN_IRIS_PKG}
	cd external && 7z x ${IGN_IRIS_PKG}

#--------------------------------------------------

iris-def: external/${IRIS_DEF_XLS} external/${IGN_IRIS_DIR}

#---------------------------------------------------------------------------

# BAN: "Base Adresse Nationale"
BAN: ; mkdir BAN

# This is 1.5 Gbytes
BAN/addok-france-bundle.zip:
	make BAN
	cd BAN && wget https://adresse.data.gouv.fr/data/ban/adresses-odbl/latest/addok/addok-france-bundle.zip

BAN/addok-data:
	make BAN/addok-france-bundle.zip
	cd BAN && unzip -d addok-data addok-france-bundle.zip


# Ubuntu 18.04
# sudo snap install docker 

BAN/docker-compose.yml:
	make BAN/addok-data
	cd BAN && wget https://raw.githubusercontent.com/etalab/addok-docker/master/docker-compose.yml

run-docker:
	make BAN/docker-compose.yml
	printf "sudo docker-compose up"

#BAN-deps:
#	sudo apt-get install redis-server python3 python3-dev python3-pip python3-virtualenv
#XXXXX:
#	make BAN
#	cd BAN && virtualenv addok --python=/usr/bin/python3.7
#	source addok/bin/activate && pip install addok addok-fr addok-france

#---------------------------------------------------------------------------
