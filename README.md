
Intented to experiment with map tools.

---

* See this [information](doc/MAKE-MESSAGE.md)


* Starting from scratch:
  * `make run-voila`
     * to run show a map (with IDRIS contours in Paris)
     * before that: it will create the Python venv with proper modules

* Running:
  * `make run-notebook` -> will run jupyter (with the proper Python venv)
   * You can see `src/MapSimpleFolium.ipynb` or `src/MapFoliumTesting.ipynb`

---
