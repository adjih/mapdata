#! /usr/bin/env python
#---------------------------------------------------------------------------

import os
import timeit
import pandas as pd

import folium

#---------------------------------------------------------------------------

class DataStore:
    def __init__(self, path):
        self.path = path

    def get_path(self, file_name):
        return os.path.join(self.path, file_name)

    def has_file(self, file_name):
        return os.path.exists(self.get_path(file_name))

    def open(self, file_name, *args, **kwargs):
        return open(self.get_path(file_name), *args, **kwargs)

    def write_file(self, file_name, content):
        with self.open(file_name, "w") as f:
            f.write(content)

    def read_file(self, file_name):
        with self.open(file_name) as f:
            return self.read()
        
#---------------------------------------------------------------------------

# Just because "print()" are ugly on 'voila' output
verbose = False
def set_verbose(new_verbose):
    global verbose
    verbose = new_verbose
        
#---------------------------------------------------------------------------

IRIS_CONTOUR_CSV = "contours-iris-2014.csv"

# TODO:
# * Automatically get and save extracted data from departments
# * manage Iris Habitat types, etc.
#    note: `set(iris.iris_outline["TYP_IRIS"])` == {'A', 'D', 'H', 'Z'}

class IrisDataManager:
    def __init__(self, store):
        self.store = store
        self.iris_data = None

    def get_store_dir(self):
        return self.store.path
        
    def _load_data(self):
        start_time = timeit.default_timer()
        csv_file_name = self.store.get_path(IRIS_CONTOUR_CSV)
        self.iris_data = pd.read_csv(csv_file_name, sep=";")
        end_time = timeit.default_timer()
        if verbose:
            print("(loaded {} in {}s)".format(csv_file_name, end_time-start_time))

    def _ensure_data(self):
        if self.iris_data is None:
            self._load_data()
    
    def get_data(self, dept_list=None):
        self._ensure_data()
        if dept_list is None:
            return self.iris_data
        else:
            str_dept_list = [str(dept) for dept in dept_list]
            return self.iris_data[self.iris_data["num_dept"].isin(str_dept_list)]
        
#---------------------------------------------------------------------------

