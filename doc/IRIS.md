# Information sur IRIS

IRIS signifie "Ilots Regroupés pour l'Information Statistique" et est défini par l'INSEE 
(voir la [Definition INSEE](https://www.insee.fr/fr/metadonnees/definition/c1523) )

*"Afin de préparer la diffusion du recensement de la population de 1999, l'INSEE avait développé un découpage du territoire en mailles de taille homogène appelées IRIS2000. Un sigle qui signifiait « Ilots Regroupés pour l'Information Statistique » et qui faisait référence à la taille visée de 2 000 habitants par maille élémentaire."*

Notes:
+ *On distingue trois types d'IRIS: Les IRIS d'habitat, Les IRIS d'habitat, Les IRIS divers (il s'agit de grandes zones spécifiques peu habitées et ayant une superficie importante)* 
+ *Le découpage en IRIS peut être affecté par les modifications de la géographie communale (fusions de communes, créations ou rétablissements de communes, échanges de parcelles). Aussi il est utile de spécifier son année de référence en notant par exemple : IRIS-géographie 1999 ou IRIS-géographie 2008.*

---

## Contours IRIS

Ils sont disponibles via l'IGN, et référenciés par data.gouv.fr, etc.
* [Contours….IRIS®](https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#contoursiris) sur le 
site de l'IGN, (actuellement: années 2013 à 2019) 
* Visualiser la [Carte IRIS sur GeoPortail](https://www.geoportail.gouv.fr/donnees/contours-iris) (lien de S.G.)
* Pointeurs [IRIS Data.gouv.fr](https://www.data.gouv.fr/fr/datasets/contours-iris/)

Apparemment il y a quelque problèmes mineurs, en particulier de noms (accentués/non accentués) [1], et de topologie(?).

Des cartes sont ré-exportées: faire par exemple sur IRIS data.gouv.fr, une
[recherche sur "contours iris"](https://www.data.gouv.fr/fr/search/?q=contours+iris)


Certains sont sur data.gouv.fr, par exemple:
* [Contours IRIS (2014)](https://www.data.gouv.fr/fr/datasets/contours-iris-2014/) apparement re-partagés par GRDF
(avec la note *"Certains contours ont été recalculés car ils ne validaient pas les standards OGC."*), 
aussi sur le [Site Opendata GRDF](https://opendata.grdf.fr/explore/dataset/contours-iris-2014/information/)
  * aux formats [CSV](https://www.data.gouv.fr/fr/datasets/r/5e2dbbb6-2a2d-42e8-9b42-210669045869), 
JSON, GeoJSON, Shapefile, ... 


---
Meta-information des contours IRIS 2020 ()

---

### CSV du [Contours IRIS (2014)](https://www.data.gouv.fr/fr/datasets/contours-iris-2014/) 

|                  | 0                             | 1                             |
|:-----------------|:------------------------------|:------------------------------|
| DEPCOM           | 49188                         | 53080                         |
| NOM_COM          | Marcé                         | Couptrain                     |
| IRIS             | 0                             | 0                             |
| DCOMIRIS         | 491880000                     | 530800000                     |
| NOM_IRIS         | Marcé                         | Couptrain                     |
| TYP_IRIS         | Z                             | Z                             |
| geo_point_2d     | 47.5822005195,-0.300105685151 | 48.4820005273,-0.291900315595 |
| num_dept         | 49                            | 53                            |
| NOM_DEPT         | MAINE-ET-LOIRE                | MAYENNE                       |
| NOM_REGION       | PAYS DE LA LOIRE              | PAYS DE LA LOIRE              |
| P12_POP          | 849.0                         | 127.0                         |
| CODE_REGION_2016 | 52.0                          | 52.0                          |
| CODE_REGION      | 52.0                          | 52.0                          |

with additional field `GEO_DATA`


---

### Type d'IRIS (voir [page INSEE](https://www.insee.fr/fr/information/2438155))


*"On distingue trois types d'IRIS :
* les IRIS d'habitat (code H) : leur population se situe en général entre 1 800 et 5 000 habitants. Ils sont homogènes quant au type d'habitat et leurs limites s'appuient sur les grandes coupures du tissu urbain (voies principales, voies ferrées, cours d'eau, ...) ;
* les IRIS d'activité (code A) : ils regroupent environ 1 000 salariés et comptent au moins deux fois plus d'emplois salariés que de population résidente ;
* les IRIS divers (code D) : il s'agit de grandes zones spécifiques peu habitées et ayant une superficie importante (parcs de loisirs, zones portuaires, forêts, ...).
* Pour les communes non découpées en IRIS, le type de l'IRIS est codé à Z."


```import collections
collections.Counter(iris.iris_outline["TYP_IRIS"])

Counter({'Z': 72460, 'H': 23901, 'A': 1413, 'D': 596})
```

---

[1] *"A. M., 10 décembre 2019 - Bonjour,
J’ai des problèmes de caractères accentués dont je ne sais pas si ils sont liés directement à des erreurs dans la donnée ou à l’utilisation d’un mauvais encodage pour la lecture. J’utilise ogr2ogr de cette manière:
ogr2ogr -f geojson -lco RFC7946=yes -t_srs EPSG:2154 iris.geojson CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2019-10-00583/CONTOURS-IRIS_2-1_SHP_LAMB93_FXX-2019
The geojson file is produced but with bad unicode chars in content, for example:
{ “type”: “Feature”, “properties”: { “INSEE_COM”: “11417”, “NOM_COM”: “Villarzel-du-Raz\u001as”, “IRIS”: “0000”, “CODE_IRIS”: “114170000”, “NOM_IRIS”: “Villarzel-du-Raz\u001as”, “TYP_IRIS”: “Z” }, …
J’ai lu quelque part dans une doc que l’encodage était ISO 8859-15, mais le fichier CONTOURS-IRIS.cpg contient “UTF-8”, j’ai essayé de changer ça mais sans succès.
Quelqu’un a une idée ?"*

 