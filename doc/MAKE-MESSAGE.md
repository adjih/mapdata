[[NOTE: an independent Python venv (virtual environment) is used, separate from
  the current Python, and packages are installed there.]]
Usage (you will need ~1GB disk):
- `make run-voila`
   to run a map (with IDRIS contours in Paris)

This is not needed but:
- `make venv-install`
   to create+populate an independent Python venv, separate from current Python
   (automatically done with 'make run-voila')
- `source mapenv/bin/activate`
  in order to activate the Python venv in your prompt
- `make run-notebook`
  in order to run a Jupyter notebook in the Python venv
