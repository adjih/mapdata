
---------------------------------------------------------------------------

* ipywidgets
https://medium.com/@jdchipox/how-to-interact-with-jupyter-33a98686f24e

* ipyleaflet 
  - interactive

* folium (alternative to ipyleaflet)
  - not interactive
  + can save a map

---------------------------------------------------------------------------

** IRIS
https://www.insee.fr/fr/metadonnees/definition/c1523

** IGN - 

Telechargement de la maille IRIS
https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#contoursiris

Maille IRIS Edition 2019
ftp://Contours_IRIS_ext:ao6Phu5ohJ4jaeji@ftp3.ign.fr/CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z.001
 - sudo apt install p7zip
 - cp 
 - p7zip -d CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z

** IRIS
https://www.data.gouv.fr/fr/datasets/contours-iris/


https://www.data.gouv.fr/fr/search/?q=contours+iris


** Contours 2017, extraction
https://www.data.gouv.fr/fr/datasets/r/64dc362e-cdd1-4148-bdd6-17fdde26b27a


---------------------------------------------------------------------------

Scan 1000 of France map (maybe can be used instead of tiles?)

ftp://SCAN_1000_ext:Jee3choh6iezooce@ftp3.ign.fr/SCAN1000_2-1__JP2-E100_LAMB93_FXX_2020-02-01.7z.001

---------------------------------------------------------------------------

* "Contour des IRIS INSEE tout en un"
  - "Ce jeu de données a été publié le 28 avril 2015 et mis à jour le 28 avril 2015 à l'initiative et sous la responsabilité de Axel H"
  - https://www.data.gouv.fr/fr/datasets/contour-des-iris-insee-tout-en-un/

  - Ressource communautaires: "Contours géographiques des IRIS en un seul fichier et dans une seule projection (WGS84). Mise à jour à partir des contours 2018 des IRIS", Logo ARCEP, 

* Logiciel OpenSource GeoZones
  - https://www.data.gouv.fr/fr/datasets/geozones/

* "Découpage IRIS combiné aux limites communales OpenStreetMap"
  - "Mise en cohérence des polygones IRIS de Contours IRIS 2013 et des limites de communes d'OpenStreetMap simplifiées à 5m."
  - Ce jeu de données a été publié le 8 juillet 2015
  - et mis à jour le 17 juillet 2015 à l'initiative et sous la responsabilité de V. de C.-T.
  - https://www.data.gouv.fr/fr/datasets/decoupage-iris-combine-aux-limites-communales-openstreetmap/

* http://ignmap.ign.fr/

* https://www.data.gouv.fr/fr/datasets/contours-iris-2014/

* https://www.data.gouv.fr/fr/datasets/contours-iris-2016-version-2-1-france-dom-en-wgs84/

GeoJSON
https://www.data.gouv.fr/fr/datasets/r/2a7a2f2f-71b6-4283-a236-62a0375745a8

---------------------------------------------------------------------------



---------------------------------------------------------------------------

https://github.com/python-visualization/folium/issues/604

---------------------------------------------------------------------------

Serving Tiles or maps

https://github.com/mapserver/mapserver

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

https://github.com/etalab/addok-docker

Install: 
https://github.com/etalab/addok-docker/README.md

---------------------------------------------------------------------------
